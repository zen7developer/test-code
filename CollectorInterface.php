<?php

namespace App\Piggy\Stats\Exporting;

/**
 * Interface CollectorInterface
 */
interface CollectorInterface
{
    /**
     * @param array $fields
     * @param \DateTime $fromDate
     * @param \DateTime $toDate
     * @return array
     */
    public function collectStats(array $fields, \DateTime $fromDate, \DateTime $toDate): array;

    /**
     * @param array $settings
     * @return mixed
     */
    public function setSettings(array $settings);

    /**
     * @return bool
     */
    public function hasValidSettings(): bool;
}