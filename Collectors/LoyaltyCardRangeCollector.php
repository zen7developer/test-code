<?php

namespace App\Piggy\Stats\Exporting\Collectors;

use App\Piggy\Enum\CardType;
use App\Piggy\Models\Interfaces\CardInterface;
use App\Piggy\Repositories\Interfaces\CardRepositoryInterface;
use DateTime;
use App\Piggy\Stats\Exporting\CollectorInterface;
use App\Piggy\Models\Interfaces\AccountInterface;
use App\Piggy\Models\Interfaces\GiftcardInterface;

/**
 * Class LoyaltyRangeCollector
 *
 * @package App\Piggy\Stats\Exporting\Collectors
 */
class LoyaltyCardRangeCollector implements CollectorInterface
{
    /**
     * @var array
     */
    private $settings = [];

    /**
     * @var CardRepositoryInterface
     */
    private $cardRepository;

    /**
     * LoyaltyRangeCollector constructor.
     *
     * @param CardRepositoryInterface $cardRepository
     */
    public function __construct(CardRepositoryInterface $cardRepository)
    {
        $this->cardRepository = $cardRepository;
    }

    /**
     * @param array $fields
     * @param DateTime $fromDate
     * @param DateTime $toDate
     * @return array
     */
    public function collectStats(array $fields, DateTime $fromDate, DateTime $toDate): array
    {
        return $this->cardRepository->findStatsForTypeAndLimit($fields, CardType::PHYSICAL, $this->settings['limit']);
    }

    /**
     * @param array $settings
     * @return mixed|void
     */
    public function setSettings(array $settings)
    {
        $this->settings = $settings;
    }

    /**
     * @return bool
     */
    public function hasValidSettings(): bool
    {
        return isset($this->settings['limit']) && is_int($this->settings['limit']);
    }
}