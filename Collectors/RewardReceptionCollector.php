<?php

namespace App\Piggy\Stats\Exporting\Collectors;

use App\Piggy\Models\Interfaces\ShopInterface;
use App\Piggy\Repositories\Interfaces\RewardReceptionRepositoryInterface;
use App\Piggy\Repositories\Interfaces\RewardRepositoryInterface;
use App\Piggy\Repositories\Interfaces\ShopRepositoryInterface;

/**
 * Class RewardReceptionCollector
 */
class RewardReceptionCollector implements CollectorInterface
{
    /**
     * @var array
     */
    private $settings = [];

    /**
     * @var ShopRepositoryInterface
     */
    private $shopRepository;

    /**
     * @var RewardReceptionRepositoryInterface
     */
    private $rewardReceptionRepository;

    /**
     * RewardReceptionCollector constructor.
     *
     * @param ShopRepositoryInterface            $shopRepository
     * @param RewardReceptionRepositoryInterface $rewardReceptionRepository
     */
    public function __construct(ShopRepositoryInterface $shopRepository, RewardReceptionRepositoryInterface $rewardReceptionRepository)
    {
        $this->shopRepository = $shopRepository;
        $this->rewardReceptionRepository = $rewardReceptionRepository;
    }

    /**
     * @param array $fields
     * @param \DateTime $fromDate
     * @param \DateTime $toDate
     * @return array
     */
    public function collectStats(array $fields, \DateTime $fromDate, \DateTime $toDate): array
    {
        return $this->rewardReceptionRepository->findStatsForShopByPeriod($fields, $this->settings['shop'], $fromDate, $toDate);
    }

    /**
     * @param array $settings
     * @return mixed|void
     */
    public function setSettings(array $settings)
    {
        $this->settings = $settings;
    }

    /**
     * @return bool
     */
    public function hasValidSettings(): bool
    {
        return isset($this->settings['shop']) && $this->settings['shop'] instanceof ShopInterface;
    }
}