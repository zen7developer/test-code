<?php

namespace App\Piggy\Stats\Exporting;

use Illuminate\Filesystem\FilesystemAdapter;

/**
 * Class Exporter
 */
class Exporter
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var ViewerInterface
     */
    private $viewer;

    /**
     * @var CollectorInterface
     */
    private $statsCollector;

    /**
     * @var array
     */
    private $mapping;

    /**
     * @var \DateTime
     */
    private $fromDate;

    /**
     * @var \DateTime
     */
    private $toDate;

    /**
     * @var FilesystemAdapter
     */
    private $filesystem;

    /**
     * Exporter constructor.
     * @param int $id
     * @param CollectorInterface $statsCollector
     * @param ViewerInterface $viewer
     * @param FilesystemAdapter $filesystem
     * @param array $mapping
     * @param \DateTime $fromDate
     * @param \DateTime $toDate
     */
    public function __construct(
        int $id,
        CollectorInterface $statsCollector,
        ViewerInterface $viewer,
        FilesystemAdapter $filesystem,
        array $mapping,
        \DateTime $fromDate,
        \DateTime $toDate
    ) {
        $this->id = $id;
        $this->statsCollector = $statsCollector;
        $this->viewer = $viewer;
        $this->filesystem = $filesystem;
        $this->mapping = $mapping;
        $this->fromDate = $fromDate;
        $this->toDate = $toDate;
    }

    /**
     * @return string
     */
    public function export(): string
    {
        $stats = $this->statsCollector->collectStats(array_values($this->mapping), $this->fromDate, $this->toDate);
        $pathToFile = $this->viewer->renderViewIntoFile($stats, $this->mapping, $this->filesystem, $this->id . uniqid());

        return $pathToFile;
    }
}