<?php

namespace App\Piggy\Stats\Exporting;

use Illuminate\Contracts\Filesystem\Factory;
use Illuminate\Filesystem\FilesystemAdapter;

/**
 * Class ExporterBuilder
 */
class ExporterBuilder
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var ViewerInterface
     */
    private $viewer;

    /**
     * @var CollectorInterface
     */
    private $statsCollector;

    /**
     * @var array
     */
    private $mapping;

    /**
     * @var \DateTime
     */
    private $fromDate;

    /**
     * @var \DateTime
     */
    private $toDate;

    /**
     * @var FilesystemAdapter
     */
    private $filesystem;

    /**
     * ExporterBuilder constructor.
     * @param Factory $filesystemFactory
     * @param CsvViewer $csvViewer
     */
    public function __construct(Factory $filesystemFactory, CsvViewer $csvViewer)
    {
        $this->mapping = [];
        $this->filesystem = $filesystemFactory->disk('local_upload');
        $this->viewer = $csvViewer;
    }

    /**
     * @param array $settings
     * @return $this
     */
    public function setCollectorSettings(array $settings)
    {
        $this->statsCollector->setSettings($settings);

        return $this;
    }

    /**
     * @return Exporter
     * @throws \Exception
     */
    public function build()
    {
        if (!$this->id || !$this->viewer || !$this->statsCollector || !$this->mapping || !$this->fromDate || !$this->toDate) {
            throw new \Exception('Exporter is not fully initialized');
        }

        if (!$this->statsCollector->hasValidSettings()) {
            throw new \Exception('Stats collector has invalid settings');
        }

        return new Exporter(
            $this->id,
            $this->statsCollector,
            $this->viewer,
            $this->filesystem,
            $this->mapping,
            $this->fromDate,
            $this->toDate
        );
    }

    /**
     * @param ViewerInterface $viewer
     * @return ExporterBuilder
     */
    public function setViewer(ViewerInterface $viewer): ExporterBuilder
    {
        $this->viewer = $viewer;

        return $this;
    }

    /**
     * @param string $name
     * @param string $value
     * @return ExporterBuilder
     */
    public function addMappingField(string $name, string $value): ExporterBuilder
    {
        $this->mapping[$name] = $value;

        return $this;
    }

    /**
     * @param array $mapping
     * @return ExporterBuilder
     */
    public function setMappings(array $mapping): ExporterBuilder
    {
        $this->mapping = $mapping;

        return $this;
    }

    /**
     * @param CollectorInterface $collector
     * @return ExporterBuilder
     */
    public function setStatsCollector(CollectorInterface $collector): ExporterBuilder
    {
        $this->statsCollector = $collector;

        return $this;
    }

    /**
     * @param \DateTime $fromDate
     * @return ExporterBuilder
     */
    public function setFromDate(\DateTime $fromDate): ExporterBuilder
    {
        $this->fromDate = $fromDate;

        return $this;
    }

    /**
     * @param \DateTime $toDate
     * @return ExporterBuilder
     */
    public function setToDate(\DateTime $toDate): ExporterBuilder
    {
        $this->toDate = $toDate;

        return $this;
    }

    /**
     * @param FilesystemAdapter $filesystem
     * @return ExporterBuilder
     */
    public function setFilesystem(FilesystemAdapter $filesystem): ExporterBuilder
    {
        $this->filesystem = $filesystem;

        return $this;
    }

    /**
     * Id is used in generation of file, it can be any unique value for, like customerId. Created for avoiding collisions
     *
     * @param int $id
     * @return $this
     */
    public function setId(int $id)
    {
        $this->id = $id;

        return $this;
    }
}