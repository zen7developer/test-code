<?php

namespace App\Piggy\Stats\Exporting;

use Illuminate\Filesystem\FilesystemAdapter;

/**
 * Interface ViewerInterface
 */
interface ViewerInterface
{
    /**
     * @param array $data
     * @param array $mapping
     * @param FilesystemAdapter $filesystem
     * @param string $fileName
     * @return string
     */
    public function renderViewIntoFile(array $data, array $mapping, FilesystemAdapter $filesystem, string $fileName): string;
}