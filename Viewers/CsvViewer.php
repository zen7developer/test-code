<?php

namespace App\Piggy\Stats\Exporting;

use Illuminate\Filesystem\FilesystemAdapter;

/**
 * Class CsvViewer
 */
class CsvViewer implements ViewerInterface
{
    /**
     * {@inheritdoc}
     */
    public function renderViewIntoFile(array $data, array $mapping, FilesystemAdapter $filesystem, string $fileName): string
    {
        $fileName = $fileName . '.csv';
        $columnNames = array_keys($mapping);
        $filesystem->put($fileName, '');
        $handle = fopen($filesystem->path($fileName), 'w');

        // Add UTF8 BOM information
        fprintf($handle, chr(0xEF) . chr(0xBB) . chr(0xBF));

        // Add CSV headers
        fputcsv($handle, $columnNames, ';');

        foreach ($data as $row) {
            $formattedRow = [];
            foreach ($mapping as $columnName => $fieldName) {
                $formattedRow[$columnName] = $row[$fieldName];
            }
            fputcsv($handle, $formattedRow, ';');
        }

        fclose($handle);

        return $filesystem->path($fileName);
    }
}