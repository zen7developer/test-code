<?php

namespace App\Piggy\Stats\Exporting;

use Barryvdh\Snappy\PdfWrapper;
use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\View\Factory;

/**
 * Class PdfViewer
 */
class PdfViewer implements ViewerInterface
{
    /**
     * @var Factory
     */
    private $viewFactory;

    /**
     * @var PdfWrapper
     */
    private $pdfGenerator;

    /**
     * PdfViewer constructor.
     * @param Factory $viewFactory
     * @param PdfWrapper $pdfGenerator
     */
    public function __construct(Factory $viewFactory, PdfWrapper $pdfGenerator)
    {
        $this->viewFactory = $viewFactory;
        $this->pdfGenerator = $pdfGenerator;
    }

    /**
     * {@inheritdoc}
     */
    public function renderViewIntoFile(array $data, array $mapping, FilesystemAdapter $filesystem, string $fileName): string
    {
        $rows = [];
        foreach ($data as $dataRow) {
            $formattedRow = [];
            foreach ($mapping as $columnName => $fieldName) {
                $formattedRow[] = $dataRow[$fieldName];
            }
            $rows[] = $formattedRow;
        }
        $htmlTemplate = $this->viewFactory->make('common.pdf_table', ['columns' => array_keys($mapping), 'rows' => $rows])->render();
        $filesystem->put($fileName . '.pdf',  $this->pdfGenerator->loadHTML($htmlTemplate)->output());

        return $filesystem->path($fileName . '.pdf');
    }
}