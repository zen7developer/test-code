<?php

namespace App\Piggy\Stats\Exporting;

use Illuminate\Filesystem\FilesystemAdapter;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

/**
 * Class XlsxViewer
 */
class XlsxViewer implements ViewerInterface
{
    /**
     * {@inheritdoc}
     */
    public function renderViewIntoFile(array $data, array $mapping, FilesystemAdapter $filesystem, string $fileName): string
    {
        $rows = [];
        $rows[] = array_keys($mapping);

        foreach ($data as $dataRow) {
            $formattedRow = [];
            foreach ($mapping as $columnName => $fieldName) {
                $formattedRow[] = $dataRow[$fieldName];
            }
            $rows[] = $formattedRow;
        }
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getActiveSheet()->fromArray($rows);
        $writer = new Xlsx($spreadsheet);
        $writer->save($filesystem->path($fileName . '.xlsx'));

        return $filesystem->path($fileName . '.xlsx');
    }
}