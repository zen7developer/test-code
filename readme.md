An example of a simple component which written to be reusable around project for different types of stats with the same interface. There were more different selectors and exporters, I just put there the most simple for an example.

Example of a usage:
```php
$exporter = $exporterBuilder
            ->setStatsCollector($rewardReceptionCollector)
            ->setId(auth()->id())
            ->setViewer($exportViewer)
            ->setCollectorSettings(['shop' => $shop])
            ->setMappings([
                'period (year-weeknumber)' => 'period',
                'reward name' => 'title',
                'count' => 'count',
                'cost_price' => 'costPriceSum',
                'total credits' => 'creditsSum'
            ])
            ->setFromDate($fromDate)
            ->setToDate($toDate)
            ->build();
```